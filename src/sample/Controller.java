package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by AleksDev on 17.01.2018
 */

public class Controller {
    // initialising the buttons
    @FXML
    private Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12;
    // creating buttons' array
    private Button[] arrayButtons;

    // boolean value tells us if user selected two buttons  to compare images
    private boolean firstPressedButton = true;
    // last but one button's variables
    private int idSelectedButtonFirst;
    private int idImageFirst;

    @FXML
    void initialize(){
        // adding buttons to the array
        initializeButtonArray();

        // creating a list of images
        // assigning an image to each button basing on the collection
        List<Integer> listImage = new LinkedList<>();
        // adding two same images to the collection
        for (int i = 0; i < 2; i++) {
            // for better performance, division is done before loop
            int sizeArrayButtons = (int)(arrayButtons.length * 0.5f);
            //creating id of images including copies, so divided by two
            for (int j = 0; j < sizeArrayButtons; j++) {
                listImage.add(j);
            }
        }
        // shuffling images
        Collections.shuffle(listImage);

        // initialising buttons
        for (int i = 0; i < arrayButtons.length; i++) {
            final int j = i;
            //response on the press
            arrayButtons[i].setOnAction(e -> {
                //running the method which is comparing images
                buttonSelect(j, listImage.get(j));
            });
        }
    }

    // Game Engine:
    // response on the selected button
    // first value: id of the button; second value: id of the image
    private void buttonSelect(int idSelectedButton,int idImage){
        // first button selection
        // saves the state to compare it with other one if it's going to be selected
        if(firstPressedButton){
            firstPressedButton = false;

            idSelectedButtonFirst = idSelectedButton;
            idImageFirst = idImage;

            setImageButton(idSelectedButton,idImage);
        }
        // if other button was selected to compare, it checks if both images are same
        else {
            setImageButton(idSelectedButtonFirst, idImageFirst);
            firstPressedButton = true;
            // if both images are same, turn off from the game area both buttons
            if (idImage == idImageFirst && idSelectedButton != idSelectedButtonFirst){
                setImageButton(idSelectedButton, idImageFirst);
                arrayButtons[idSelectedButton].setDisable(true);
                arrayButtons[idSelectedButtonFirst].setDisable(true);

            }else {
                // if selected images are different, hide  the last but one selected image
                arrayButtons[idSelectedButtonFirst].setGraphic(null);
                arrayButtons[idSelectedButton].setGraphic(null);
                //method recursion in order to get the last button
                buttonSelect(idSelectedButton, idImage);
            }
        }
    }

    // creating a collection of buttons
    private void initializeButtonArray() {
        arrayButtons = new Button[]{button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12};
    }

    // setting buttons' image
    private void setImageButton(int idButton, int idImage){
        Image imageDecline = new Image(getClass().getResourceAsStream(idImage + ".jpg"));
        arrayButtons[idButton].setGraphic(new ImageView(imageDecline));
    }
}
