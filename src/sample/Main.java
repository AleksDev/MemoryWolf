package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Created by AleksDev on 17.01.2018
 */

public class Main extends Application {
    private Button[] btns = new Button[5];

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Parent

       // initBtnsArray();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        //root.getChildren().add(getGrid());


        primaryStage.setTitle("Memory Wolf by AleksDev v1.0");
        primaryStage.setScene(new Scene(root, 500, 600));
        primaryStage.show();
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private Pane getGrid() {
        int i = 0;
        GridPane gridPane = new GridPane();
        for(Button b : btns) {
            // do something with your button
            // maybe add an EventListener or something
            gridPane.add(b, i*(i+(int)b.getWidth()), 0);
            i++;
        }
        return gridPane;
    }

    private void initBtnsArray() {
        for(int i = 0; i < btns.length; i++) {
            btns[i] = new Button("Button-"+i);
        }
    }
}